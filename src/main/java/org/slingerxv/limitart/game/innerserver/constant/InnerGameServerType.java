package org.slingerxv.limitart.game.innerserver.constant;

public class InnerGameServerType {
	/**
	 * 战斗服类型
	 */
	public static final int SERVER_TYPE_GAME = 1;
	/**
	 * 游戏服类型
	 */
	public static final int SERVER_TYPE_FIGHT = 2;
}
