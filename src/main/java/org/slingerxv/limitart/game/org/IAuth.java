package org.slingerxv.limitart.game.org;

/**
 * 权限
 * 
 * @author hank
 *
 */
public interface IAuth {
	int getAuthID();
}
