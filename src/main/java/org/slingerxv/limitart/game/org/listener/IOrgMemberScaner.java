package org.slingerxv.limitart.game.org.listener;

import org.slingerxv.limitart.game.org.OrgMember;

public interface IOrgMemberScaner {
	void scan(OrgMember member);
}
