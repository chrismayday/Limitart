package org.slingerxv.limitart.net.http.constant;

public enum QueryMethod {
	POST, GET
}
