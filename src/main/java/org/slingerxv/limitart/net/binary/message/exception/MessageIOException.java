package org.slingerxv.limitart.net.binary.message.exception;

public class MessageIOException extends Exception {
	private static final long serialVersionUID = 1L;

	public MessageIOException(String info) {
		super(info);
	}
}
