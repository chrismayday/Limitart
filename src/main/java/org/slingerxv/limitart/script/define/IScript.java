package org.slingerxv.limitart.script.define;

public interface IScript<KEY> {
	KEY getScriptId();
}
