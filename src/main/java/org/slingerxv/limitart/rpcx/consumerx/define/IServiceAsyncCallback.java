package org.slingerxv.limitart.rpcx.consumerx.define;

/**
 * RPC异步回调
 * 
 * @author Hank
 *
 */
public interface IServiceAsyncCallback {

	void action(Object returnVal);

}
