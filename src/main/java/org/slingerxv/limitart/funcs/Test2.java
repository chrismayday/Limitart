package org.slingerxv.limitart.funcs;

public interface Test2<T1, T2> {
	boolean test(T1 t1, T2 t2);
}
