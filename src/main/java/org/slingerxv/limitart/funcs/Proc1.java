package org.slingerxv.limitart.funcs;

public interface Proc1<T> {
	void run(T t);
}
