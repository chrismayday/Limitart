package org.slingerxv.limitart.funcs;

public interface Proc2<T1, T2> {
	void run(T1 t1, T2 t2);
}
